__author__ = 'sreejith'


from base import BaseChannel
from pyslack import SlackClient


class SlackChannel(BaseChannel):
    """
    Implements slack channel for updating to slack
    """
    def __init__(self, name, api_token, slack_channel, *args, **kwargs):
        """

        :param name: Descriptive name for the channel.  This will be used as the username for the message if not
                        given
        :type name: str
        :param api_token: API token according to http://api.slack.com/
        :type api_token: str
        :param slack_channel: Name of the slack channel.  '#' will be prefixed if not already given
        :type slack_channel: str
        """
        super(self.__class__, self).__init__(name)
        self._api_token = api_token
        self.slack_channel = slack_channel
        self._client = SlackClient(self._api_token)
        self.username = kwargs.get('username', name)

    @property
    def slack_channel(self):
        return self._slack_channel

    @slack_channel.setter
    def slack_channel(self, channel_name):
        self._slack_channel = channel_name if channel_name[0] == "#" else \
            "#{}".format(channel_name)

    def update(self, message):
        """
        Updates the slack channel with the message
        :param message:
        :param username:
        :return: If the posting was successful
        :rtype: bool
        """
        return self._client.chat_post_message(self._slack_channel,
                                       str(message),
                                       username=self.username).get('ok', False)

