__author__ = 'sreejith'

from base import BaseChannel
from pyhall.python_hall import Hall

class HallChannel(BaseChannel):
    """
    Implements hall channel for updating to hall.com
    """

    def __init__(self, name, api_token, *args, **kwargs):
        super(self.__class__, self).__init__(name)
        self._api_token = api_token
        self._client = Hall(self._api_token);
        self.username = kwargs.get('username', self.name)

    def update(self, message):
        return self._client.send(self.username, message)
