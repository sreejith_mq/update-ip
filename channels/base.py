__author__ = 'sreejith'

from abc import ABCMeta, abstractmethod

class BaseChannel(object):
    """
    Defines the base channel for updating the status
    """

    __metaclass__ = ABCMeta

    def __init__(self, name):
        self.name = name

    @abstractmethod
    def update(self, message):
        raise NotImplemented
