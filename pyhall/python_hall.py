#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    python-hall.py

    The main functionality of python-hall is all here

"""
import simplejson
import requests
from .exceptions import IncorrectRoomKey

URL = 'https://hall.com/api/1/services/generic/'


class Hall(object):
    ' Instatiate a Hall object for a room use Hall.send() to send messages '

    def __repr__(self):
        return '<Hall - %s>' % self.room_key

    def __init__(self, room_key, image=None):
        self.room_key = room_key
        self.url = URL + self.room_key
        self.image = image

    def add_image(self, image):
        self.image = image

    def _post(self, bundle):
        ' Wraps around requests.posts '
        headers = {'content-type': 'application/json'}
        payload = simplejson.dumps(bundle)
        result = requests.post(self.url, headers=headers, data=payload)
        if result.status_code == 201:
            return True
        if result.status_code == 404:
            raise IncorrectRoomKey()

    def send(self, title, message):
        if not self.room_key:
            raise IncorrectRoomKey()
        bundle = {'title': title, 'message': message}
        if self.image:
            bundle['picture'] = self.image
        return self._post(bundle)
