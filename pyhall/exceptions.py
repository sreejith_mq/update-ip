#!/usr/bin/env python
# -*- coding: utf-8 -*-


class IncorrectRoomKey(Exception):
    ' Raised when an incorrect room key is passed '
    pass
