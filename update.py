#!/usr/bin/python
__author__ = 'sreejith'

from ipgetter import myip
from importlib import import_module
import channels
from pdb import set_trace


class Updater(object):
    """
    Gets the IP and updates the channel through the IP
    """

    IP_FILE_NAME = '.{}.ip.lock'
    def __init__(self, name,
                 channel_class,
                 update_always=False,
                 *channel_args,
                 **channel_kwargs):
        """

        :param name: Name for the updater.  This will be used as the name for the channel also
        :type name: str
        :param channel_class: channel class to be used for update.  This can be a class name relative to the
                                module `channels` or a fully qualified class name
        :type channel_class: str
        :param update_always: Whether to update the IP always or only when changed
        :type update_always: bool
        :param channel_args: Additional arguments for the channel
        :param channel_kwargs: Additional key word arguments for the channel
        """
        self.name = name
        self.update_always = update_always
        self.channel = self._import_class(channel_class)(name, *channel_args, **channel_kwargs)

    def _import_class(self, name):
        """
        Imports a class
        :param name: Name of the class.  This can be a class name relative to the
                                module `channels` or a fully qualified class name
        :type name: str
        :return: The class
        :rtype: type
        """
        # First determine if the class is relative to channels
        mod_name, class_name = self._split_module(name)
        if not mod_name:
            mod_name = 'channels'
        mod = import_module(mod_name)
        return getattr(mod, class_name)

    @property
    def save_file(self):
        return self.IP_FILE_NAME.format(self.name)

    @classmethod
    def _split_module(cls, name):
        split = name.split('.')
        return '.'.join(split[:-1]), split[-1]

    def _get_message(self, ip):
        return "Server IP address is {}".format(ip) if ip else \
                "Unable to get server IP address"

    def _save_ip(self, ip):
        with open(self.save_file, 'w') as ip_f:
            ip_f.write(ip)

    def _should_update(self, ip):
        try:
            with open(self.save_file, 'r') as ip_f:
                previous_ip = ip_f.read()
        except IOError:
            return True
        return previous_ip != ip

    def update(self):
        """
        Updates the ip to the channel
        :return:
        """
        success = False
        attempts = 0
        ip = ''
        while attempts < 5 and not ip:
            ip = myip()
            attempts += 1
        if self.update_always or self._should_update(ip):
            print "Updating the ip to {}".format(self.name)
            success = self.channel.update(self._get_message(ip))
            self._save_ip(ip)
        else:
            print "Skipping the update to {} since the IP has not changed".format(self.name)
            success = True

        return  success


def update_stack(update_always=False):
    slack_class = 'channels.slack.SlackChannel'
    slack_api = 'xoxp-2358815231-2725925421-2727331688-4f48eb'
    slack_arguments = {
                        'api_token': slack_api,
                       'slack_channel': 'server-ip',
                       'username': 'server'
                        }
    updater = Updater('slack',slack_class , update_always, **slack_arguments)
    updater.update()

def update_hall(update_always=False):
    hall_class = 'channels.hall.HallChannel'
    hall_api = '01b630aae8e259ba89adcef9e301556c'
    hall_arguments = {
                        'api_token': hall_api
                        }
    updater = Updater('hall', hall_class, update_always, **hall_arguments)
    updater.update()


if __name__ == '__main__':
    # API key for info@mquotient.net in slack
    update_always = False
    update_stack(update_always)
    update_hall(update_always)





